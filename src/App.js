import React, {Component} from 'react';
import {Navbar, NavItem, Footer} from 'react-materialize';
import {Route, Switch, NavLink} from 'react-router-dom'
import HomePage from './containers/HomePage/HomePage';
import AboutUs from './containers/AboutUs/AboutUs';
import Contacts from './containers/Contacts/Contacts';
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Navbar brand='University of Kyrgyzstan' right>
                    <NavLink
                        exact
                        to="/"
                        activeClassName="selected"
                    >Home page</NavLink>
                    <NavLink
                        to="/about-us"
                        activeClassName="selected"
                    >About us</NavLink>

                    <NavLink
                        to="/contacts"
                        activeClassName="selected"
                    >Contacts</NavLink>
                </Navbar>

                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/about-us" component={AboutUs}/>
                    <Route path="/contacts" component={Contacts}/>
                </Switch>

                <Footer copyrights="&copy 2015 University of Kyrgyzstan"
                        links={
                            <ul>
                                <li>
                                    <NavLink
                                        exact
                                        to="/"
                                    >Home page</NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        to="/about-us"
                                    >About us</NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        to="/contacts"
                                    >Contacts</NavLink>
                                </li>
                            </ul>
                        }
                        className='example'
                >
                    <h5 className="white-text">University of Kyrgyzstan</h5>
                    <p className="grey-text text-lighten-4">You can use rows and columns here to organize your footer
                        content.</p>
                </Footer>
            </div>
        );
    }
}

export default App;
