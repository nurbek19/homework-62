import React, {Component, Fragment} from 'react';
import {Col, Card} from 'react-materialize';
import university from '../../images/university1.jpg';
import './HomePage.css';

class HomePage extends Component {
    render() {
        return (
            <Fragment>
                <div className="university-title">
                    <img src={university} alt="image"/>
                    <h1>University of your dream!</h1>
                </div>

                <div className="news">
                    <h2>News</h2>
                    <Col m={6} s={12}>
                        <Card className='blue-grey' textClassName='white-text' title='Lorem ipsum'
                              actions={[<a href='#'>This is a link</a>]}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab aliquam amet aperiam
                            consequuntur distinctio dolor dolorem doloribus ea earum est facere hic laborum nam natus,
                            nihil nisi obcaecati officia perspiciatis quis reprehenderit sunt tempora unde, velit
                            voluptatum. A asperiores eaque excepturi ipsum odio quae quidem quis quo ratione ullam!
                        </Card>
                    </Col>

                    <Col m={6} s={12}>
                        <Card className='blue-grey' textClassName='white-text' title='Lorem ipsum'
                              actions={[<a href='#'>This is a link</a>]}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab aliquam amet aperiam
                            consequuntur distinctio dolor dolorem doloribus ea earum est facere hic laborum nam natus,
                            nihil nisi obcaecati officia perspiciatis quis reprehenderit sunt tempora unde, velit
                            voluptatum. A asperiores eaque excepturi ipsum odio quae quidem quis quo ratione ullam!
                        </Card>
                    </Col>

                    <Col m={6} s={12}>
                        <Card className='blue-grey' textClassName='white-text' title='Lorem ipsum'
                              actions={[<a href='#'>This is a link</a>]}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab aliquam amet aperiam
                            consequuntur distinctio dolor dolorem doloribus ea earum est facere hic laborum nam natus,
                            nihil nisi obcaecati officia perspiciatis quis reprehenderit sunt tempora unde, velit
                            voluptatum. A asperiores eaque excepturi ipsum odio quae quidem quis quo ratione ullam!
                        </Card>
                    </Col>
                </div>
            </Fragment>

        )
    }
}

export default HomePage;