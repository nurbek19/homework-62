import React, {Component} from 'react';
import {Tabs, Tab} from 'react-materialize';
import './AboutUs.css';

class AboutUs extends Component {
    render() {
        return (
            <div className="about-us">
                <h2>About us</h2>

                <Tabs className='tab-demo z-depth-1'>
                    <Tab title="About us" active>
                        <div className="description-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, distinctio dolorem
                            doloribus eius error illo libero magnam neque nesciunt nisi obcaecati odio officia,
                            provident quae, quas ratione vero! Expedita inventore maxime non officiis provident sed
                            temporibus voluptas? Aspernatur at consequatur dicta doloremque eligendi molestias mollitia,
                            nemo nostrum obcaecati odio officiis optio quasi quo reprehenderit tempora? Dignissimos
                            necessitatibus obcaecati possimus, provident quis repellat voluptatibus. Cum ea fugiat
                            laudantium maiores reprehenderit? Accusantium adipisci animi at atque debitis dignissimos
                            doloribus, eaque earum est eum excepturi id magnam maxime minima minus porro, quae sint,
                            temporibus ullam unde velit veniam voluptate. A accusantium, atque blanditiis consequuntur
                            culpa debitis delectus deserunt dolore doloremque eaque enim error exercitationem facilis
                            harum illo impedit iste itaque modi mollitia natus nesciunt nisi nulla officia optio
                            pariatur perferendis provident quasi qui quia recusandae repellat repudiandae sequi suscipit
                            tempora velit vitae voluptatum. Cupiditate eum neque nostrum pariatur veritatis! Aspernatur
                            assumenda, beatae dolores eos id illum, impedit ipsum itaque laboriosam maxime molestiae,
                            non officia optio praesentium quas quibusdam quis repudiandae saepe soluta ut vel vero
                            voluptate voluptatem? Alias, aliquam aspernatur blanditiis dolore eligendi enim error
                            excepturi exercitationem fugiat ipsa itaque labore mollitia necessitatibus nulla, omnis
                            provident quisquam rerum sequi, tempora vitae. Consequuntur, itaque.
                        </div>
                    </Tab>

                    <Tab title="Our Stuff">
                        <div className="description-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci blanditiis
                            debitis delectus deserunt ea esse est, eveniet fuga impedit iure libero mollitia pariatur
                            quia quidem sunt ut? Blanditiis quo sed vero! Adipisci aspernatur deserunt dolores doloribus
                            fugiat inventore labore laboriosam magnam nisi obcaecati, officiis pariatur quam quasi quos,
                            similique soluta sunt? Adipisci deserunt dolorum ducimus eius magnam nihil nobis, odit
                            officia perspiciatis, porro quas qui sint, ullam. Adipisci distinctio dolorum ducimus ea et
                            ex fuga itaque labore maiores mollitia natus nisi odio pariatur quibusdam rerum tempora unde
                            ut, vero! Eaque molestiae mollitia neque officiis perferendis sed vero. Asperiores,
                            blanditiis consectetur culpa cupiditate delectus dicta dolorem doloremque ducimus earum
                            eveniet ex excepturi exercitationem fuga id illo inventore iure magni minus modi nemo nisi
                            obcaecati officia pariatur quae quaerat qui quo ratione, recusandae reiciendis saepe
                            similique soluta totam ullam ut veniam vitae voluptates. Atque deserunt eos optio quasi
                            quisquam recusandae sapiente sequi voluptatem! Alias animi aperiam consectetur deleniti
                            dignissimos ducimus eos est et, excepturi exercitationem facilis fuga id incidunt ipsam
                            ipsum labore laboriosam maxime molestias natus neque nisi nostrum odit officia optio
                            perspiciatis possimus provident quis repudiandae sint soluta sunt velit voluptate
                            voluptatum! Animi delectus hic illo officia quibusdam repellendus, voluptatem!
                        </div>
                    </Tab>

                    <Tab title="Our goals">
                        <div className="description-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, distinctio dolorem
                            doloribus eius error illo libero magnam neque nesciunt nisi obcaecati odio officia,
                            provident quae, quas ratione vero! Expedita inventore maxime non officiis provident sed
                            temporibus voluptas? Aspernatur at consequatur dicta doloremque eligendi molestias mollitia,
                            nemo nostrum obcaecati odio officiis optio quasi quo reprehenderit tempora? Dignissimos
                            necessitatibus obcaecati possimus, provident quis repellat voluptatibus. Cum ea fugiat
                            laudantium maiores reprehenderit? Accusantium adipisci animi at atque debitis dignissimos
                            doloribus, eaque earum est eum excepturi id magnam maxime minima minus porro, quae sint,
                            temporibus ullam unde velit veniam voluptate. A accusantium, atque blanditiis consequuntur
                            culpa debitis delectus deserunt dolore doloremque eaque enim error exercitationem facilis
                            harum illo impedit iste itaque modi mollitia natus nesciunt nisi nulla officia optio
                            pariatur perferendis provident quasi qui quia recusandae repellat repudiandae sequi suscipit
                            tempora velit vitae voluptatum. Cupiditate eum neque nostrum pariatur veritatis! Aspernatur
                            assumenda, beatae dolores eos id illum, impedit ipsum itaque laboriosam maxime molestiae,
                            non officia optio praesentium quas quibusdam quis repudiandae saepe soluta ut vel vero
                            voluptate voluptatem? Alias, aliquam aspernatur blanditiis dolore eligendi enim error
                            excepturi exercitationem fugiat ipsa itaque labore mollitia necessitatibus nulla, omnis
                            provident quisquam rerum sequi, tempora vitae. Consequuntur, itaque.
                        </div>
                    </Tab>

                    <Tab title="Programs">
                        <div className="description-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, distinctio dolorem
                            doloribus eius error illo libero magnam neque nesciunt nisi obcaecati odio officia,
                            provident quae, quas ratione vero! Expedita inventore maxime non officiis provident sed
                            temporibus voluptas? Aspernatur at consequatur dicta doloremque eligendi molestias mollitia,
                            nemo nostrum obcaecati odio officiis optio quasi quo reprehenderit tempora? Dignissimos
                            necessitatibus obcaecati possimus, provident quis repellat voluptatibus. Cum ea fugiat
                            laudantium maiores reprehenderit? Accusantium adipisci animi at atque debitis dignissimos
                            doloribus, eaque earum est eum excepturi id magnam maxime minima minus porro, quae sint,
                            temporibus ullam unde velit veniam voluptate. A accusantium, atque blanditiis consequuntur
                            culpa debitis delectus deserunt dolore doloremque eaque enim error exercitationem facilis
                            harum illo impedit iste itaque modi mollitia natus nesciunt nisi nulla officia optio
                            pariatur perferendis provident quasi qui quia recusandae repellat repudiandae sequi suscipit
                            tempora velit vitae voluptatum. Cupiditate eum neque nostrum pariatur veritatis! Aspernatur
                            assumenda, beatae dolores eos id illum, impedit ipsum itaque laboriosam maxime molestiae,
                            non officia optio praesentium quas quibusdam quis repudiandae saepe soluta ut vel vero
                            voluptate voluptatem? Alias, aliquam aspernatur blanditiis dolore eligendi enim error
                            excepturi exercitationem fugiat ipsa itaque labore mollitia necessitatibus nulla, omnis
                            provident quisquam rerum sequi, tempora vitae. Consequuntur, itaque.
                        </div>
                    </Tab>
                </Tabs>
            </div>
        )
    }
}

export default AboutUs;
