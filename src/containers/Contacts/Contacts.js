import React, {Component} from 'react';
import {Row, Input, Button} from 'react-materialize';
import './Contacts.css';

class Contacts extends Component {
    render() {
        return (
            <div className="contacts">
                <div className="get-in-touch">
                    <h2>Subscribe!</h2>
                    <Row>
                        <Input s={6} label="First Name"/>
                        <Input s={6} label="Last Name"/>
                        <Input type="email" label="Email" s={12}/>
                        <Button waves='light'>Send</Button>
                    </Row>
                </div>

                <ul className="address">
                    <li><h4>+996 550 055577</h4></li>
                    <li><h4>sunrisestudiopro@gmail.com</h4></li>
                    <li><h4>Bishkek city, street Kalyk Akieva 66</h4></li>
                </ul>
            </div>
        )
    }
}

export default Contacts;
